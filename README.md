# Luxo

### Une lampe de bureau motorisée et intelligente.

Dans le cadre de mon projet de fin d'année, j'ai décidé de rendre hommage à  Pixar et sa lampe humaine Luxo présente lors de leur premier court métrage.

![](./rapport.support/luxo_real)
