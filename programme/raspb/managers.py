from Adafruit_PWM_Servo_Driver import PWM
import time
import threading
from math import *
import numpy as np
from scipy.optimize import minimize
import matplotlib.pyplot as plt


class Robot :
    """ manage luxo servos """

    def __init__(self, simulate=False, address=0x40) :

        self.address = address
        self.pwm = PWM(address)
        self.pwm.setPWMFreq(60)
        self.servoMin = 150
        self.servoMax = 600
        self.simulation = simulate
        self.init_q = [0, 0]
        self.rad2angle = 180.0/pi
        self.prevAngles = {7 : 0, 0 : 0, 3 : 0}

        # Robot specifications

        self.a1 = 0.07
        self.a2 = 0.057
        self.b1 = 0.160
        self.b2 = 0.146
        self.b3 = 0.06
        self.b4 = 0.077


        self.T12 = lambda q1 : np.array([[cos(q1), -sin(q1), self.a1], [sin(q1), cos(q1),  self.b1], [0, 0, 1]])
        self.T23 = lambda q2 : np.array([[cos(q2), -sin(q2), self.a2], [sin(q2), cos(q2), -self.b2], [0, 0, 1]])
        self.T34 = lambda q3 : np.array([[cos(q3), -sin(q3),  0], [sin(q3), cos(q3),  self.b3], [0, 0, 1]])
        self.T4M = np.array([[1, 0, 0], [0, 1, self.b4], [0, 0, 1]])

    def _setServo(self, channel, angle) :
        """ Move servo on channel to the new angle value """
        def smouth(self, channel, angle) :
            prev_angle = self.prevAngles[channel]

            course = np.linspace(prev_angle, angle, int(abs(prev_angle - angle)/2) )
            for curr_angle in course :    
                pulse = curr_angle*(self.servoMax-self.servoMin)/180 + self.servoMin
                self.pwm.setPWM(channel, 0, int(pulse))
                time.sleep(0.02)
            self.prevAngles[channel] = angle

        thread = threading.Thread(None, smouth, None, (self, channel, angle), {})
        thread.start()

    def setQ0(self, q) :
        """ Move first motor """
        q = q*self.rad2angle
        q = max(0, q)
        q = min(180, q)
        self._setServo(0, q)

    def setQ1(self, q) :
        """ Move second motor """
        q = -q*self.rad2angle + 168
        q = max(0, q)
        q = min(180, q)
        self._setServo(7, q)

    def setQ2(self, q) :
        """ Move third motor """
        q = -q*self.rad2angle + 32
        q = max(0, q)
        q = min(180, q)
        self._setServo(0, q)
    
    def setQ3(self, q=0) :
        """ Move fourth motor """
        q = -q*self.rad2angle + 108
        q = max(40, q)
        q = min(180, q)
        self._setServo(3, q)
    
    def setAngles(self, q1, q2, q3) :
        """ set a robot position by its angles """
        if self.simulation :
            self.simulate([q1, q2, q3])
        else : 
            if q1 != None :
                self.setQ1(q1)
            if q2 != None :
                self.setQ2(q2)
            if q3 != None :
                self.setQ3(q3)

        if q1 == 0 and q2 == 0 and q3 == 0 :
            self.pwm = PWM(self.address)
            self.pwm.setPWMFreq(60)

    def move(self, x, y, t) :
        """ move robot at point (x, y) with a t orientation angle in radian """
        
        q = self.MGI(x, y, t)
        self.setAngles(q[0], q[1], q[2])
    
    def MGI(self, x, y, t) :
        """ Compute Inverse Geometric model """
        ep = 1
        W = self.b3
        X = self.a1 - x - self.b4*sin(t)
        Y = self.b1 - y + self.b4*cos(t)
        z1 = self.a2
        z2 = self.b2
        x0 = 2*(Y*z1 + X*z2)
        y0 = 2*(X*z1- Y*z2)
        z0 = W**2 - X**2 -Y**2 -z1**2 -z2**2
               
        delta = x0**2 + y0**2 -z0**2
        if delta < 0 :
            return [0, 0, 0]

        si = (z0*x0 + ep*y0*sqrt(delta))/(x0**2+y0**2)
        ci = (z0*y0 - ep*x0*sqrt(delta))/(x0**2+y0**2)
        if z0 == 0 :
            if ep == 1 :
                q1 = np.arctan2(-y0, x0)
            else :
                q1 = np.arctan2(-y0, x0) + pi
        else :
            q1 = np.arctan2(si, ci)
                                                                                                  
        y1 =  X*ci + Y*si + z1
        y2 =  X*si - Y*ci + z2
        q2 = np.arctan2(y1, y2)
                                                                                                                                               
        q3 = t-q1-q2

        return [q1, q2, q3]


    def wait(self) :
        """ Come back robot at the waiting position """
        self.setQ3(0)
        self.setQ2(0)
        time.sleep(2)
        self.setQ1(0)

        self.pwm = PWM(self.address)
        self.pwm.setPWMFreq(60)


    def simulate(self, q) :
        """ Draw robot in a graphical window """
        # Remove least figure
        plt.clf()
    
        # Draw first arm
        plt.xlim([-0.01, 0.5])
        plt.ylim([0, 0.5])
        plt.plot([0, 0], [0, self.b1], 'k-', lw=2, color='r')
        plt.plot([0, self.a1], [self.b1, self.b1], 'k-', lw=2, color='r')
         
        # Draw second arm
        O2p = np.array([[0], [-self.b2], [1]])
        O2p = np.dot(self.T12(q[0]), O2p)
        plt.plot([self.a1, O2p[0]], [self.b1, O2p[1]], 'k-', lw=2, color='b')
        
        O3 = np.array([[self.a2], [-self.b2], [1]])
        O3 = np.dot(self.T12(q[0]), O3)
        plt.plot([O2p[0], O3[0]], [O2p[1], O3[1]], 'k-', lw=2, color='b')
        
        # Draw third arm
        O4 = np.array([[0], [self.b3], [1]])
        O4 = self.T12(q[0]).dot( self.T23(q[1]).dot( O4 ))
        plt.plot([O3[0], O4[0]], [O3[1], O4[1]], 'k-', lw=2, color='g')
        
        # Draw four arm
        M = np.array([[0], [0], [1]])
        M = self.T12(q[0]).dot( self.T23(q[1]).dot( self.T34(q[2]).dot( self.T4M.dot( M ))))
        plt.plot([O4[0], M[0]], [O4[1], M[1]], 'k-', lw=2)
        
        plt.show()







class Lights :
    """ manage luxo leds """

    def __init__(self, adress=0x40) :
        
        self.pwm = PWM(0x40)
        self.pwm.setPWMFreq(60)

    def setLight(self, value) :
        """ action lights on channel with a pourcent value og brightness """
        value = max(0, value)
        value = min(1, value)
        on = int((1 - value)*4045)
        off = 4095
        self.pwm.setPWM(12, on, off)

        if value == 0 :
            self.pwm = PWM(0x40)
            self.pwm.setPWMFreq(60)
