#! /usr/bin/python
from network import Server
from managers import Robot, Lights
import time
from math import pi

def listenerServer(mes, client) :
    print("message : "+mes)
    tab = mes.split(':')
    if tab[0] == "light" :
        value = float(tab[1])
        light.setLight(value)

    if tab[0] == "on" :
        robot.setQ2(-pi/4)
        robot.setQ1(pi/5)
        robot.setQ3(-pi/6)
        time.sleep(0.5)
        light.setLight(1)

    if tab[0] == "off" :
        robot.wait()
        time.sleep(1)
        light.setLight(0)

    if tab[0] == "move" :
        x = float(tab[1])
        y = float(tab[2])
        t = float(tab[3])

        robot.move(x, y, t)

    if tab[0] == "angles" :
        q1 = float(tab[1])
        q2 = float(tab[2])
        q3 = float(tab[3])

        robot.setAngles(q1, q2, q3)


    client.stop()

if __name__ == '__main__' :
    server = Server(2017)
    server.addListener(listenerServer)


    light = Lights()
    robot = Robot()
    
    ans = ""
    while ans != "fin" :
        ans = raw_input("Taper fin pour arreter : ")
    server.stop()
