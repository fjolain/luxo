#! /usr/bin/python

import threading
import socket
import sys


def run() :
    arg = sys.argv
    connect = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connect.connect(('localhost', int(arg[1]) ))

    connect.send(arg[2].encode())
    connect.close()


if __name__ == '__main__' :
    thread = threading.Thread(None, run, None, (), {})
    thread.start()
