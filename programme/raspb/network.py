#! /usr/bin/python3
# -*- coding: utf-8 -*-

import socket
import threading
import select
import os
import time


class Connection:
    """ This class is client socket connection. It calls all listener when message is received """
    

    def __init__(self, param1, param2=None) :
        """ Manage a single connection.
            Client(ip, port)    : create a connection to a server
            Client(connect)     : create a connection from a server.accept
        """
        self.connect = None
        # If connection is from a server.accept instance
        if param2 == None :
            self.connect = param1
        # If connection is to a server
        else :
            self.connect = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.connect.connect((param1, param2))

        self.listeners = []
        self.resume = True
        thread = threading.Thread(None, self.loopMessage, None, (), {})
        thread.start()
        print("[Network]\tConnection started")

    def addListener(self, listener) :
        """ Add listener to catch new message"""
        self.listeners.append(listener)

    def deletH4eListener(self, listener) :
        """ Delete listener to stop catching new message"""
        if listener in self.listeners :
            self.listeners.remove(listener)

    def update(self, mes, connection) :
        """ call all listener with the new message"""
        for listener in self.listeners :
            listener(mes, connection)

    def loopMessage(self) :
        """endless loop listening messages """
        while self.resume :
            connections, wlist, xlist = select.select([self.connect], [], [], 0.05)
            for connect in connections :
                data = connect.recv(4096).decode()
                # Remove char used to fill buffer
                #data = data.replace('\00', '')
                data = str(data).replace('\n', '')
                if data != '' :
                    self.update(data, self)
            time.sleep(0.001)
        self.connect.close()

    def send(self, mes) :
        return self.connect.send(mes.encode())

    def fileno(self) :
        """ Used for select.select """
        return self.connect.fileno()


    def stop(self) :
        self.resume = False
        print("[Network]\tconnection stopped")


class Server :
    """ This class listen on a port and create new connection.
        It's build as a Singleton pattern.
    """

    class __impl :

        def __init__(self, port) :
            """ Begin server listening """
            self.connections = []
            self.listeners = []
            self.resume = True

            # setup connexion
            self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.server.bind(('', int(port) ))
            self.server.listen(5)

            # launch new connection Thread
            thread = threading.Thread(None, self.__loopNewConnection, None, (), {})
            thread.start()
            
            print("[Network]\tlisten on port "+str(port))


        def __loopNewConnection(self) :
            while self.resume :
                # wait for a new connection
                connection, info = self.server.accept()
                # add client
                c = Connection(connection)
                c.listeners = self.listeners
                self.connections.append(c)


        def addListener(self, listener) :
            """ Add listener to all connections"""
            # Update own listeners list
            self.listeners.append(listener)
            # update connections listeners list
            for connection in self.connections :
                connection.addListener(listener)

        def deleteListener(self, listener) :
            """ Delete listener to all connections"""
            # Update own listeners list
            self.listeners.remove(listener)
            # update connections listeners list
            for connection in self.connections :
                conection.addListener(listener)

        def stop(self) :
            self.resume = False
            self.server.close()
            for connection in self.connections :
                connection.stop()
            print("[Network]\tlistening stopped")
            # This kill the thread, I don't find a softer way :(
            os.kill(os.getpid(), 9)

        def id(self) :
           return id(self)


    __instance = None

    def __init__(self, port=2016) :
        if Server.__instance is None :
            Server.__instance = Server.__impl(port)
        self.__dict__['_Singleton__instance'] = Server.__instance

    def __getattr__(self, attr) :
        return getattr(self.__instance, attr)
    def __setattr__(self, attr, value) :
        return setattr(self.__instance, attr, value)




def listenerServer(mes, client) :
    print(mes)
    client.send(mes)
    client.stop()

if __name__ == '__main__' :
    server = Server(2016)
    server.addListener(listenerServer)
    
    ans = ""
    while ans != "fin" :
        ans = input("Taper fin pour arreter : ")
    server.stop()
